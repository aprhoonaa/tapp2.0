

import java.sql.Statement;
import java.sql.PreparedStatement;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Encuesta extends javax.swing.JFrame {

    /**
     * Creates new form MiniEncuesta
     */
    public Encuesta() {
        loadDriver();
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        rbWindows = new javax.swing.JRadioButton();
        rbLinux = new javax.swing.JRadioButton();
        rbMac = new javax.swing.JRadioButton();
        cbProgra = new javax.swing.JCheckBox();
        cbDiseno = new javax.swing.JCheckBox();
        cbAdmon = new javax.swing.JCheckBox();
        slHoras = new javax.swing.JSlider();
        lbHoras = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(204, 0, 0));

        jLabel1.setText("Elige un sistema operativo");

        jLabel2.setText("Elige la especialidad");

        jLabel3.setText("Horas que dedicas en la computadora");

        buttonGroup1.add(rbWindows);
        rbWindows.setSelected(true);
        rbWindows.setText("Windows");

        buttonGroup1.add(rbLinux);
        rbLinux.setText("Linux");

        buttonGroup1.add(rbMac);
        rbMac.setText("Mac");

        cbProgra.setText("Programación");

        cbDiseno.setText("Diseño Gráfico");

        cbAdmon.setText("Administración");

        slHoras.setMaximum(23);
        slHoras.setValue(4);
        slHoras.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                slHorasStateChanged(evt);
            }
        });

        lbHoras.setText("4");

        jButton1.setText("Generar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lbHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(slHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(cbAdmon)
                            .addComponent(cbDiseno)
                            .addComponent(cbProgra)
                            .addComponent(rbMac)
                            .addComponent(rbLinux)
                            .addComponent(rbWindows)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(jButton1)))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(rbWindows)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbLinux)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbMac)
                .addGap(22, 22, 22)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(cbProgra)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbDiseno)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbAdmon)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(slHoras, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbHoras, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(28, 28, 28)
                .addComponent(jButton1)
                .addGap(53, 53, 53))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void slHorasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_slHorasStateChanged
        // TODO add your handling code here:
        this.lbHoras.setText(Integer.toString(this.slHoras.getValue()));
    }//GEN-LAST:event_slHorasStateChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        String aSisOp[] = {"Windows","Linux","Mac"};
        String aEsp[] = {"Programación","Diseño Gráfico","Administración"};
        
        String sSisOp = "";
        char cProg = 'N';
        char cDiseno = 'N';
        char cAdmon = 'N';
        int  iHoras = 0;
        
        String mensaje = "";
        if (this.rbWindows.isSelected()){
            mensaje += aSisOp[0];
            sSisOp = aSisOp[0];
        }
        if (this.rbLinux.isSelected()){
            mensaje += aSisOp[1];
            sSisOp = aSisOp[1];
        }
        if (this.rbMac.isSelected()){
            mensaje += aSisOp[2];
            sSisOp = aSisOp[2];
        }        
        mensaje += ",";
        if (this.cbProgra.isSelected()){
            mensaje += aEsp[0];
            cProg = 'S';
        }
        mensaje += ",";
        if (this.cbDiseno.isSelected()){
            mensaje += aEsp[1];
            cDiseno = 'S';
        }  
        mensaje += ",";
        if (this.cbAdmon.isSelected()){
            mensaje += aEsp[2];
            cAdmon = 'S';
        }
        
        mensaje += ","+Integer.toString(this.slHoras.getValue());
        iHoras = this.slHoras.getValue();
        
        JOptionPane.showMessageDialog(this, mensaje);
        
        this.guardarArchivo(mensaje);
        this.guardarBD(sSisOp,cProg,cDiseno,cAdmon,iHoras);
        
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Encuesta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Encuesta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Encuesta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Encuesta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Encuesta().setVisible(true);
            }
        });
    }
    
    void guardarArchivo(String mensaje){
        FileWriter archivo;
        
        try {
            archivo = new FileWriter("respuestas.txt",true);
            
            archivo.write(mensaje+"\n");
            
            archivo.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Encuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            // handle the error
            JOptionPane.showMessageDialog(this,"Error en la carga del driver");
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox cbAdmon;
    private javax.swing.JCheckBox cbDiseno;
    private javax.swing.JCheckBox cbProgra;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel lbHoras;
    private javax.swing.JRadioButton rbLinux;
    private javax.swing.JRadioButton rbMac;
    private javax.swing.JRadioButton rbWindows;
    private javax.swing.JSlider slHoras;
    // End of variables declaration//GEN-END:variables

    private void guardarBD(String sSisOp, char cProg, char cDiseno, char cAdmon, int iHoras) {
        Connection conn = null;
        String connString = "jdbc:mysql://10.25.0.21:3306/encuesta?zeroDateTimeBehavior=convertToNull&";

        try {
            conn = DriverManager.getConnection(connString +"user=encuesta&password=encuesta");
 
            String insSQL = "INSERT INTO respuestas (sisoper,progra,diseno,admon,horas) VALUES (?,?,?,?,?)";

            PreparedStatement pstmt = conn.prepareStatement(insSQL);
            
            pstmt.setString(1, sSisOp);
            pstmt.setString(2, String.valueOf(cProg));
            pstmt.setString(3, String.valueOf(cDiseno));
            pstmt.setString(4, String.valueOf(cAdmon));
            pstmt.setInt(5, iHoras);
            
            pstmt.execute();
   
            conn.close();
            
           
           
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } 
        
          //To change body of generated methods, choose Tools | Templates.
    }
}
